# React Avançado

- Data de Início : 01-07-2020
- Data de Término : xx-xx-xxxx

Código desenvolvido no curso React Avançado

### Tecnologias Utilizadas:

Backend com Strapi
FrontEnd com Next

- [ReactJs](http://teste.com.br)
- [NodeJs]()
- [Strapi - Node.js Headless CMS](https://strapi.io/)
  - Vantagens :
    - 100% open-source
    - Altamente customizável(API e Painel)
    - Funciona com Rest API ou GraphQL
    - Self Hosted ( você controla seus dados e onde colocá-los )
    - RoadMap bem definido e grande investimento
  - Features :
    - Suporte multiBanco ( SQLite / MongoDB / PostGres / MariaDB )
    - Documentação automática com plugin de one-click
    - Integração a Webhooks
    - Integração com diferentes serviços ( Cloudinary, SenGrid, Algolia, Redis )
    - Sistema de Emails
    - Sistema de assets que otimiza imagens e cria em diferentes tamanhos
  - O Que podemos fazer com Strapi :
    - Sites estáticos
    - Sites Institucionais para empresas;
    - Sites de Notícias
    - Ecommerce
  - Porque utilizar o Strapi :
    - Mais Agilidade
    - Codebase toda em Javascript( mais fácil manutenção )
    - Sem mensalidade e controle do próprio dado( Contentful, DatoCMS )
  - Como Funciona :
    - Possui 3 tipos de estrutura de dados :
      - Collection Types ( conjunto de dados padrão : EX : usuarios, produtos , categorias )
      - Single Types ( dado único : homepage, footer, menu )
      - Component Types ( estrutura de dados reutilizadas : galerias, hero )

* [Jest - Delightful JavaScript Testing Framework](https://jestjs.io/)
* [React Testing Library]()
* [Cypress](https://www.cypress.io/)
  ```
  $ yarn add cypress
  ```
* [Yarn](https://yarnpkg.com/)
* []()
* []()
* []()
* []()
* []()
* []()
* []()
* []()
* []()
* []()

### Início da Criação do Projeto

- Criando o Projeto

```
// 1 - Criando o Projeto
$ yarn create next-app

// 2 - Criando arquivo de Configuração do TypeScript
$ touch tsconfig.json

// 3 - Instalando dependências do Typescript
$ yarn add --dev typescript @types/react @types/node

// 4 - Instalando o ESLint
$ npx eslint --init

// 5 - Instalar
$ yarn add --dev eslint-plugin-react@latest @typescript-eslint/eslint-plugin@latest @typescript-eslint/parser@latest eslint@latest

// 6 -
$ yarn add eslint-plugin-react-hooks --dev

// 7 - 


// 8 - 
$ yarn add --dev husky lint-staged 

// 9 - 
$ arn add --dev jest @babel/preset-typescript @types/jest

```
